## Important 

To use this program you must already have the ESRI package software already downloaded to your C:\ drive.  Additionally, in order to run this code, you must use the MOJO compile and run tools: 
- **cd C:\esri\MOJ20\examples**
- **moj_compile progname.java**
- **moj_run progname .**

## QuickStartXY.java Information
The aim of this project was to create map software where the user could select a point layer in the table of contents, use a new "PickIcon" tool in the toolbar, and navigate to an image within an opened dialog window. Once the user selects an image, the points in the selected point layer will render that image.  This new PickIcon tool is present in the toolbar, menu, and has it's own custom tooltip.